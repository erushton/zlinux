IDENTIFICATION DIVISION.
PROGRAM-ID. HELLO-WORLD.
*> simple hello world program
PROCEDURE DIVISION.
    DISPLAY 'Hello GitLab! Would you like to play a nice game of chess?'.
    STOP RUN.
